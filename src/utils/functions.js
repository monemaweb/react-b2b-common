export const isNumeric = function (obj) {
  return !Array.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0
}
