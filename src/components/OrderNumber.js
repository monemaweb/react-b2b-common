import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

/**
 *
 * @param customer_num global progressive number of customer
 * @param customer_order_num progressive number of customer order
 * @returns {string} a string of the type POXXXXXXXX
 */

const orderNum = (customerNum, customerOrderNum) => {
  if (customerNum && Number.isInteger(customerNum) && customerOrderNum && Number.isInteger(customerOrderNum)) {
    return 'PO'.concat(parseInt(customerNum.toString().concat(customerOrderNum.toString().padStart(4, '0'))).toString(16).padStart(8, '0').toUpperCase())
  }
  return ''
}

const OrderNumber = props => {
  const orderNumber = orderNum(props.customerNum, props.customerOrderNum)
  return (
    <Fragment>
      {orderNumber}
    </Fragment>
  )
}

OrderNumber.propTypes = {
  customerNum: PropTypes.number.isRequired,
  customerOrderNum: PropTypes.number.isRequired
}

export default OrderNumber
