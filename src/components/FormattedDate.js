import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

/**
 *
 * @param date a date object
 * @returns {string} a localized date string
 */

const formatDate = (date, locale = 'it-IT') => {
  if (date && (Object.prototype.toString.call(date) === '[object Date]')) {
    return new Intl.DateTimeFormat(locale, {
      year: 'numeric',
      month: 'short',
      day: '2-digit'
    }).format(date)
  }
  return ''
}

const FormattedDate = (props) => {
  const formattedDate = formatDate(props.date, props.locale)
  return <Fragment>{formattedDate}</Fragment>
}

FormattedDate.propTypes = {
  date: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  locale: PropTypes.string
}
export default FormattedDate
