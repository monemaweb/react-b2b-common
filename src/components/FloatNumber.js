import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { isNumeric } from '../utils/functions'

/**
 *
 * @param num number to be formatted
 * @returns {string} a number rounded to 2nd decimal formatted with it-IT locale
 */

const formatFloat = (num, locale = 'it-IT') => {
  if (num && isNumeric(num)) {
    return new Intl.NumberFormat(locale, {
      style: 'decimal',
      maximumFractionDigits: 2,
      minimumFractionDigits: 2
    }).format(num)
  }
  return ''
}

const FormattedFloat = props => {
  const FormattedFloat = formatFloat(props.num, props.locale)
  return (
    <Fragment>
      {FormattedFloat}
    </Fragment>
  )
}

FormattedFloat.propTypes = {
  num: PropTypes.number.isRequired,
  locale: PropTypes.string
}

export default FormattedFloat
