import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { isNumeric } from '../utils/functions'

/**
 *
 * @param num number to be formatted as currency
 * @returns {string} a number rounded to 2nd decimal with euro currency symbol formatted with it-IT locale
 */

const formattedPrice = (num, currency, locale = 'it-IT') => {
  if (num && isNumeric(num)) {
    return new Intl.NumberFormat(locale, {
      style: 'currency',
      maximumFractionDigits: 2,
      minimumFractionDigits: 2,
      currency: currency
    }).format(num)
  }
  return ''
}

const Currency = props => {
  const formattedValue = formattedPrice(props.num, props.currency, props.locale)
  return (
    <Fragment>
      { formattedValue }
    </Fragment>
  )
}

Currency.propTypes = {
  num: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  currency: PropTypes.string.isRequired,
  locale: PropTypes.string
}
export default Currency
