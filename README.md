# monema/react-b2b-common

> 

[![NPM](https://img.shields.io/npm/v/@monema/react-b2b-common.svg)](https://www.npmjs.com/package/@monema/react-b2b-common) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save @monema/react-b2b-common
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from '@monema/react-b2b-common'

class Example extends Component {
  render () {
    return (
      <MyComponent />
    )
  }
}
```

## License

MIT © [atasso](https://github.com/atasso)
